Sistema de de admistracion de solicitudes de transporte de automoviles:

Requerimientos para la ejecucion:
-JDK
-Intellij IDEA CE


Pasos una ves el hambiente de trabajo este listo:
-Build para obtener dependencias
-Ejecutar


Webservices url: http://localhost:8080

Solicitud:
    -Agregar solicitud: /solicitud/add (POST)
    -Obtener todas las solicitudes: /solicitud/all (GET)
    -Borrar solicitud: /solicitud/delete (DELETE)
Auto
    -Agregar solicitud: /auto/add (POST)
    -Obtener todas las solicitudes: /auto/all (GET)
    -Borrar solicitud: /auto/delete (DELETE)
Transporte:
    -Agregar transporte: /transporte/add (POST)
    -Obtener todas las transporte: /transporte/all (GET)
    -Borrar transporte: /transporte/delete (DELETE)
    
    
    
Ejemplo webservice para agregar una nueva solicitud:

Con este webservice se guarda la solicitud y sus dependientes con ayuda de  JPA usado la especificacion cascada en la cual toma en cuenta las tablas relacionadas "transporte" y "autos".
Por lo tanto a la hora de usar el webservice "/solicitud/all" respondera con un JSON en el cual vendra la solicitud y los datos dependientes, la implmentacion viene en el modelo con la anotacion
@OneToMany.

http://localhost:8080/solicitud/add (POST)

{"fecha":"","status":"Abierto","transporteList": [	
	
	{
		"empresa":"Poliacero",
		"chofer":"Juan",
		"placas":"asdf4568"
	},
	{
		"empresa":"Aceros murillo",
		"chofer":"Perdro",
		"placas":"qwas4568"	
	}
	],
	"autoList":[
		{"marca":"vw", "modelo":"golf","transmisiion":"manual","descripcionEstetica":"Gris"},
		{"marca":"vw", "modelo":"golf","transmisiion":"manual","descripcionEstetica":"Azul"},
		{"marca":"vw", "modelo":"golf","transmisiion":"manual","descripcionEstetica":"Verde"}
		]
}



